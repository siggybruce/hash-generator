import javafx.fxml.FXML;
import javafx.stage.Stage;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.security.MessageDigest;

public class Controller
{
    @FXML
    private javafx.scene.control.Button exitButton;
    @FXML
    private javafx.scene.control.TextField inputDisplay;
    @FXML
    private javafx.scene.control.TextField md5Display;
    @FXML
    private javafx.scene.control.TextField sha1Display;
    @FXML
    private javafx.scene.control.TextField sha256Display;
    @FXML
    private javafx.scene.control.TextField sha512Display;

    public void pasteButtonControl() throws Exception
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        DataFlavor dataFlavor = DataFlavor.stringFlavor;

        if (clipboard.isDataFlavorAvailable(dataFlavor))
        {
            String clipboardText = (String) clipboard.getData(dataFlavor);
            inputDisplay.setText(clipboardText);
        }
    }
    public void md5CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(md5Display.getText());
        clipboard.setContents(selection, null);
    }
    public void sha1CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(sha1Display.getText());
        clipboard.setContents(selection, null);
    }
    public void sha256CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(sha256Display.getText());
        clipboard.setContents(selection, null);
    }
    public void sha512CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(sha512Display.getText());
        clipboard.setContents(selection, null);
    }
    public void generateHash() throws Exception
    {
        md5Display.setText(getHash(inputDisplay.getText(), "MD5"));
        sha1Display.setText(getHash(inputDisplay.getText(), "SHA1"));
        sha256Display.setText(getHash(inputDisplay.getText(), "SHA-256"));
        sha512Display.setText(getHash(inputDisplay.getText(), "SHA-512"));
    }
    public void clearButtonControl()
    {
        inputDisplay.setText("");
        md5Display.setText("");
        sha1Display.setText("");
        sha256Display.setText("");
        sha512Display.setText("");
    }
    public void exitButtonControl()
    {
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.close();
    }
    private String getHash(String input, String hashType) throws Exception
    {
        MessageDigest messageDigest = MessageDigest.getInstance(hashType);
        byte[] bytes = messageDigest.digest(input.getBytes("UTF-8"));
        StringBuilder hash = new StringBuilder();

        for (int index = 0; index < bytes.length; index++)
        {
            hash.append(String.format("%02x", bytes[index]));
        }
        return hash.toString().toUpperCase();
    }
}
